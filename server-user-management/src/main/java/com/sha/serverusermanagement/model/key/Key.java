package com.sha.serverusermanagement.model.key;

import lombok.Data;

import java.io.Serializable;


@Data
public class Key implements Serializable {
    private String name;
}
