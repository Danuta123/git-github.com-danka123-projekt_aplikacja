package com.sha.serverusermanagement.service.file;

import com.sha.serverusermanagement.model.file.File;
import com.sha.serverusermanagement.repository.FileRepository;
import com.sha.serverusermanagement.service.exceptions.FileStorageException;
import com.sha.serverusermanagement.service.exceptions.MyFileNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class FileServiceImpl implements FileService{

    @Autowired
    private FileRepository fileRepository;

    @Override
    public File save(MultipartFile file) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

         try {
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            File Filedb= new File(fileName, file.getContentType(), file.getBytes());
            fileRepository.save(Filedb);
           return  Filedb;

        }catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    @Override
    public List<File> findAllFiles (){
        return this.fileRepository.findAll();
    }

    @Override
    public File getFileById(Long fileId) {
        return fileRepository.findById(fileId)
                .orElseThrow(() -> new MyFileNotFoundException("File not found with id " + fileId));
    }
}
