package com.sha.serverusermanagement.model.note;

public enum NoteStatus {
    PUBLIC,
    ENCRYPTED,
    SHARED
}
