package com.sha.serverusermanagement.model.file;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Entity
@Table(name = "files")
public class File{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @GenericGenerator(name="increment", strategy = "increment")
    private Long file_id;

    @Column (name="namename")
    private String fileName;

    @Column (name="typetype")
    private String fileType;

    //@Lob
    @Column(name="datadata")
    private byte[] data;

    @Column (name="ownerowner")
    private String owner;

    public File(){

    }

    public File(String filename, String fileType, byte[] array){
        this.data=array;
        this.fileName=filename;
        this.fileType=fileType;
        this.owner="xx";
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return this.fileName;
    }

    public Long getFile_Id() {
        return this.file_id;
    }
    public void setFile_id(Long id) {
        this.file_id=id;
    }

    public void setData(byte[] array) {
        this.data=array;
    }

    public byte[] getData() {
        return this.data;
    }

}
