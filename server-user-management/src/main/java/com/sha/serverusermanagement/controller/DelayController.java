package com.sha.serverusermanagement.controller;

import java.util.Date;

public class DelayController {

    private static final long LOCK_TIME_DURATION = (long) (0.2 * 60 * 1000);
    private Date lockTime;

    public DelayController(){
        this.lockTime=new Date();
    }

    public boolean delay(){ ;
        long currentTimeInMillis = System.currentTimeMillis();
        if (lockTime.getTime() + LOCK_TIME_DURATION < currentTimeInMillis) {
            return true;
        }
        return false;
    }

}
