package com.sha.serverusermanagement.service.note;


import com.sha.serverusermanagement.model.note.Note;
import com.sha.serverusermanagement.model.note.NoteStatus;
import com.sha.serverusermanagement.model.user.User;
import com.sha.serverusermanagement.repository.NoteRepository;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class NoteServiceImpl implements NoteService{

    @Autowired
    private NoteRepository noteRepository;

    @Override
    public Note save(Note note){
        /*tu encryptowanie wiadomości */
        if(note.getStatus()==NoteStatus.ENCRYPTED){
            String salt = "18210bce922f5b3c";//KeyGenerators.string().generateKey();
            CharSequence pass=note.getKey();
            TextEncryptor encryptor = Encryptors.delux(pass,salt);
            note.setText(encryptor.encrypt(note.getText()));
        }
        noteRepository.save(note);
        return note;
    }

    @Override
    public String decrypt(Long note_id, String key){
        String salt = "18210bce922f5b3c";//KeyGenerators.string().generateKey();
        TextEncryptor encryptor = Encryptors.delux(key,salt);
        Note note= noteRepository.findById(note_id).orElse(null);
        return encryptor.decrypt(note.getText());
    }


    @Override
    public List<Note> findByOwner(String owner){
        return this.noteRepository.findByOwner(owner);
    }

    @Override
    public Note findById(Long note_id){
        return this.noteRepository.findById(note_id).orElse(null);
    }

    @Override
    public List<Note> findAllNotes (){
        return this.noteRepository.findAll();
    }

    @Override
    public List<Note> findByStatus (NoteStatus status){return  this.noteRepository.findByStatus(status);}

    @Override
    public List<Note> getNotesById(List<Long> lista){
        return noteRepository.getNotesById(lista);
    }/*  @Override
    public Optional<Note> findByAccessed (Optional<Long> accesed){
        return noteRepository.findById(accesed);
    }*/

}
