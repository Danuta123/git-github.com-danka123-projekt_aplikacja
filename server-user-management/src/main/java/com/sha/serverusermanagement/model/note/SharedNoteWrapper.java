package com.sha.serverusermanagement.model.note;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
public class SharedNoteWrapper implements Serializable {

    private String owner;
    private String text;
    private String key;
    private NoteStatus status;
    private String accessed;

}
