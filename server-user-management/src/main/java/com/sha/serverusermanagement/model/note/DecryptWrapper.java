package com.sha.serverusermanagement.model.note;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;

@Data
public class DecryptWrapper {
    private Long note_id;
    private String key;
}
