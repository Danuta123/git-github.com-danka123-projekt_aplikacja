package com.sha.serverusermanagement.repository;

import com.sha.serverusermanagement.model.note.Note;
import com.sha.serverusermanagement.model.note.NoteStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;


public interface NoteRepository extends JpaRepository <Note,Long> {

    Optional<Note> findById(Long note_id);

   @Query("SELECT n FROM Note n WHERE n.owner = (:o)")
   List<Note> findByOwner (@Param("o") String o);

   List<Note> findByStatus(NoteStatus state);

   @Query("SELECT n FROM Note n WHERE n.note_id IN (:list)")
   List<Note> getNotesById (List<Long> list);

  /*  @Query("SELECT n FROM Note n  WHERE n.status=(:o)")
    List<Note> findByStatus (@Param("o") String o);*/
}
