package com.sha.serverusermanagement.service.note;
import com.sha.serverusermanagement.model.note.Note;
import com.sha.serverusermanagement.model.note.NotePermissions;

import java.util.List;
import java.util.Optional;

public interface NotePermissionService {

   NotePermissions save(Long note_id, String accessed);

   List<Long> findPermByAccessed(String accessed);

}
