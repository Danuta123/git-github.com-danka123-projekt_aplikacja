package com.sha.serverusermanagement.repository;

import com.sha.serverusermanagement.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;


/*ten Long,bo id usera jest formatu Long*/
public interface UserRepository extends JpaRepository <User,Long> {
    Optional<User> findByUsername (String username);

    @Query("UPDATE User u SET u.failedAttempt = ?1 WHERE u.username = ?2")
    @Modifying
    public void updateFailedAttempts(int failAttempts, String username);

    /*w parametrze lista id zwraca liste nazw użytkowników*/
    @Query("SELECT u.name FROM User u WHERE u.id in (:pIdList)")
    List <String>  findUserIdList (@Param("pIdList") List<Long> idList);
}
