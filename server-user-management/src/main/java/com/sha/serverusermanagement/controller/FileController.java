package com.sha.serverusermanagement.controller;

import com.sha.serverusermanagement.model.file.File;
import com.sha.serverusermanagement.service.file.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class FileController {

    @Autowired
    private FileService fileService;

    @PostMapping("api/file/add")
    public ResponseEntity<?> uploadFile(@RequestParam MultipartFile file) {

        File f=fileService.save(file);

        return ResponseEntity.ok(f);
        //return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("api/file/all")
    public ResponseEntity<?> getAllFiles() {
        return ResponseEntity.ok(fileService.findAllFiles());
    }
}

/*    @GetMapping("api/file/download/{fileId}")
    public ResponseEntity<?> downloadFile(@PathVariable Long fileId) {
        File f = fileService.getFileById(fileId);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(f.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + f.getFileName() + "\"")
                .body(new ByteArrayResource(f.getData()));
    }
}*/