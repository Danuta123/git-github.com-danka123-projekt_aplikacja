package com.sha.serverusermanagement.model.note;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;

@Data
public class Decrypted implements Serializable{
    private String decrypted;

    public Decrypted(){}
    public Decrypted (String text){
        this.decrypted=text;
    }

}
