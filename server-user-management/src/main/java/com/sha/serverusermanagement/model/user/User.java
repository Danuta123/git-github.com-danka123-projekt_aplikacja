package com.sha.serverusermanagement.model.user;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name="user")

public class User implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="name")
    private String name;

    @Column(name="username")
    private String username;

    @Column(name="password")
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name="role")
    private Role role;

    @Column(name="enabled")
    private boolean enabled=true;

    @Column(name = "f1")
    private boolean accountNonLocked=true;

    @Column(name = "failed_attempt")
    private int failedAttempt=0;

    @Column(name = "lock_time")
    private Date lockTime=null;

}
