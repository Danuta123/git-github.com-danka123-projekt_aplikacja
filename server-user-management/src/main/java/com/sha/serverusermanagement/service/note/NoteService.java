package com.sha.serverusermanagement.service.note;
import com.sha.serverusermanagement.model.note.Note;
import com.sha.serverusermanagement.model.note.NoteStatus;
import com.sha.serverusermanagement.model.user.User;
//import sun.util.resources.cldr.ext.LocaleNames_es_GT;

import java.util.List;
import java.util.Optional;

public interface NoteService {
    Note save(Note note);

    String decrypt(Long note_id, String key);

    Note findById(Long note_id) ;

    List<Note> findByOwner(String owner);

    List<Note> findByStatus (NoteStatus status);

    List<Note> findAllNotes();

    List<Note> getNotesById(List<Long> lista);
   // Optional<Note> findByAccessed(Optional<Long> lista);
}
