package com.sha.serverusermanagement.service.note;


import com.sha.serverusermanagement.model.note.Note;
import com.sha.serverusermanagement.model.note.NotePermissions;
import com.sha.serverusermanagement.model.note.NoteStatus;
import com.sha.serverusermanagement.repository.NotePermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class NotePermissionServiceImpl implements NotePermissionService {

    @Autowired
    private NotePermissionRepository notePermissionRepository;

    @Override
    public NotePermissions save(Long tmp, String accessed){
        NotePermissions n= new NotePermissions(tmp,accessed);
        this.notePermissionRepository.save(n);
        return n;
    }

    @Override
    public List<Long> findPermByAccessed(String accessed){
        return this.notePermissionRepository.findPermByAccessed(accessed);
    }

}
