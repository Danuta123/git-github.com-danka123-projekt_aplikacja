package com.sha.serverusermanagement.config;

import com.sha.serverusermanagement.model.user.User;
import com.sha.serverusermanagement.service.user.UserDetailsImpl;
import com.sha.serverusermanagement.service.user.UserDetailsServiceImpl;
import com.sha.serverusermanagement.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class EntryPoint extends BasicAuthenticationEntryPoint {

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private UserService userService;


    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authEx) throws IOException {

        String username = userDetailsService.getUname(); //request.getParameter("username");
        User user = userService.findByUsername(username);

         //AuthenticationException exception;
         //exception = new LockedException("Nie udalo się zalogowac!");
        String exception = "Nie udalo sie zalogowac! Niepoprawny login lub haslo";

        if (user != null) {
            if (user.isEnabled() && user.isAccountNonLocked()) {
                if (user.getFailedAttempt() < UserDetailsServiceImpl.MAX_FAILED_ATTEMPTS - 1) {
                    userDetailsService.increaseFailedAttempts(userService.findByUsername(username));
                } else {
                    userDetailsService.lock(user);
                    userDetailsService.resetFailedAttempts(user.getUsername());

                }
            } else if (!user.isAccountNonLocked()) {
                if (userDetailsService.unlockWhenTimeExpired(user)) {
                    //exception = new LockedException("Konto jest zablokowane!");
                    exception ="Konto jest zablokowane!";
                }
            }
        }

        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        PrintWriter writer = response.getWriter();
        writer.println(exception);
    }

    @Override
    public void afterPropertiesSet() {
        setRealmName("YOUR REALM");
        //super.afterPropertiesSet();
    }
}
