package com.sha.serverusermanagement.service.file;

import com.sha.serverusermanagement.model.file.File;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileService {

    public File save(MultipartFile file);
   // public File save(MultipartFile file);
    public File getFileById(Long fileId);
    List<File> findAllFiles();
}
