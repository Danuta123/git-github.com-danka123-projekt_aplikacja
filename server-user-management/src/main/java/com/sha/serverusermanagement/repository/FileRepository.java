package com.sha.serverusermanagement.repository;

import com.sha.serverusermanagement.model.file.File;
import org.springframework.data.jpa.repository.JpaRepository;


public interface FileRepository extends JpaRepository<File, Long> {

}
