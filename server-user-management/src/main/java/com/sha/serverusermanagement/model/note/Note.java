package com.sha.serverusermanagement.model.note;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name="note")
public class Note implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long note_id;

    @Column(name="owner")
    private String owner;

    @Column(name="text")
    private String text;

    @Column(name="key_")
    private String key;

    @Enumerated(EnumType.STRING)
    @Column(name="status_")
    private NoteStatus status;

    public Note(){}
    public Note (String owner,String text,NoteStatus status){
        this.owner=owner;
        this.text=text;
        this.status=status;
    }
}
