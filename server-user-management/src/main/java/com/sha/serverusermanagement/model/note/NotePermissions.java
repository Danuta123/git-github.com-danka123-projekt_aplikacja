package com.sha.serverusermanagement.model.note;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name="NotePermissions")
public class NotePermissions implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long perm_id;

    @Column(name="note_id")
    private Long note_id;

    @Column(name="accessed")
    private String accessed;

    public NotePermissions(Long note_id, String accessed){
        this.note_id=note_id;
        this.accessed=accessed;
    }

}
