import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {User} from '../model/user';
import {Note} from '../model/note';
import {NoteWrapper} from '../model/noteWrapper';
import {DecryptWrapper} from '../model/decryptWrapper';

//const API_URL = 'http://localhost:8080/api/user/';
//const API_URL2 = 'http://localhost:8080/api/note/';

const API_URL = '/api/user/';
const API_URL2 = '/api/note/';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {
  }

  login(user: User): Observable<any> {
    const headers = new HttpHeaders(user ? {
      authorization: 'Basic ' + btoa(user.username + ':' + user.password)
    } : {});

    return this.http.get<any>(API_URL + 'login', {headers})
      .pipe(map(response => {
        if (response) {
          localStorage.setItem('currentUser', JSON.stringify(response));
        }
        return response;
      }));
  }

  register(user: User): Observable<any> {
    return this.http.post(API_URL + 'registration', JSON.stringify(user),
      {headers: {'Content-Type': 'application/json; charset=UTF-8'}});
  }

  logOut(): Observable<any> {
    return this.http.post(API_URL + 'logout', {})
      .pipe(map(response => {
        localStorage.removeItem('currentUser');
      }));
  }

  findAllUsers(): Observable<any> {
    return this.http.get(API_URL + 'all',
      {headers: {'Content-Type': 'application/json; charset=UTF-8'}});
  }

  addNote(note: Note): Observable<any> {
    return this.http.post(API_URL + 'note/add', JSON.stringify(note),
      {headers: {'Content-Type': 'application/json; charset=UTF-8'}});
  }

  getUserNotes(username: string): Observable<any> {
    return this.http.get(API_URL2 + username,
      {headers: {'Content-Type': 'application/json; charset=UTF-8'}});
  }

  getPublicNotes(): Observable<any> {
    return this.http.get(API_URL2 + 'state/PUBLIC',
      {headers: {'Content-Type': 'application/json; charset=UTF-8'}});
  }

  getSentNotes(username: string): Observable<any> {
    return this.http.get(API_URL2 + 'get/' + username,
      {headers: {'Content-Type': 'application/json; charset=UTF-8'}});
  }

  sendNote(noteWrapper: NoteWrapper): Observable<any> {
    return this.http.post(API_URL + 'note/send', JSON.stringify(noteWrapper),
      {headers: {'Content-Type': 'application/json; charset=UTF-8'}});
  }

  decryptNote( decryptWrapper: DecryptWrapper): Observable<any> {
    return this.http.post( API_URL2 + 'decrypt', JSON.stringify(decryptWrapper),
      {  headers: {'Content-Type': 'application/json; charset=UTF-8'}}).pipe(
      map((data: any) => {
        return data;
      }), catchError( error => {
        return throwError( 'Something went wrong!' );
      })
    );
  }
}
