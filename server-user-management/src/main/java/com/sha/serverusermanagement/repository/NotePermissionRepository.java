package com.sha.serverusermanagement.repository;
import com.sha.serverusermanagement.model.note.Note;
import com.sha.serverusermanagement.model.note.NotePermissions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;


public interface NotePermissionRepository extends JpaRepository <NotePermissions,Long>{

  @Query("SELECT n.note_id FROM NotePermissions n WHERE n.accessed = (:o)")
  List<Long> findPermByAccessed (@Param("o") String o);

}
