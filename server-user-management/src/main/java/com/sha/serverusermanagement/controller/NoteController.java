package com.sha.serverusermanagement.controller;

import com.sha.serverusermanagement.model.note.*;
import com.sha.serverusermanagement.service.note.NotePermissionService;
import com.sha.serverusermanagement.service.note.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;



@RestController
public class NoteController {

    @Autowired
    private NoteService noteService;

    @Autowired
    private NotePermissionService notePermissionService;

    @PostMapping("api/user/note/add")
    public ResponseEntity <?> addNote(@RequestBody Note note){
        noteService.save(note);
        return new ResponseEntity<>(note, HttpStatus.CREATED);
    }

    @PostMapping("api/note/decrypt")
    public ResponseEntity <?> decryptNote(@RequestBody DecryptWrapper decryptWrapper){

        String d= noteService.decrypt(decryptWrapper.getNote_id(),decryptWrapper.getKey());
        Decrypted d2= new Decrypted(d);
       return new ResponseEntity<>(d2,HttpStatus.OK);
    }

    @PostMapping("api/user/note/send")
    public ResponseEntity<?> addAccess(@RequestBody SharedNoteWrapper wrapper){
        Note note= new Note(wrapper.getOwner(),wrapper.getText(), wrapper.getStatus());
        noteService.save(note);
        notePermissionService.save(note.getNote_id(), wrapper.getAccessed());
        return new ResponseEntity<>(note, HttpStatus.CREATED);
    }

    @GetMapping("api/note/get/{username}")
    public ResponseEntity<?> getByAccessed(@PathVariable  String username){
       List<Long> list =  notePermissionService.findPermByAccessed(username);
       return ResponseEntity.ok(noteService.getNotesById(list));
    }

    @GetMapping("api/note/{owner}")
    public ResponseEntity<?> getByOwner(@PathVariable  String owner){
        return ResponseEntity.ok(noteService.findByOwner(owner));
    }

    @GetMapping("api/note/id/{note_id}")
    public ResponseEntity<?> getByNoteId(@PathVariable Long note_id){
        return ResponseEntity.ok(noteService.findById(note_id));
    }

    @GetMapping("api/note/state/{status}")
    public ResponseEntity<?> findByStatus(@PathVariable  NoteStatus status) {
        return ResponseEntity.ok(noteService.findByStatus(status));
    }

    @GetMapping("api/note/all")
    public ResponseEntity<?> getAllNotes() {
        return ResponseEntity.ok(noteService.findAllNotes());
    }
}



