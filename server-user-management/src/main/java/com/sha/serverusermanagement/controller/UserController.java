package com.sha.serverusermanagement.controller;

import com.sha.serverusermanagement.model.user.Role;
import com.sha.serverusermanagement.model.user.User;
import com.sha.serverusermanagement.service.user.UserDetailsImpl;
import com.sha.serverusermanagement.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("api/user/registration")
    public ResponseEntity <?> register(@RequestBody User user){
        if(userService.findByUsername(user.getUsername())!=null){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        user.setRole(Role.USER);
        userService.save(user);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @GetMapping("api/user/login")
    public ResponseEntity <?> auth(Principal principal){
        DelayController delay= new DelayController();
        while(delay.delay()){}

        if(principal==null || principal.getName()==null){
            return ResponseEntity.ok(principal);
        }
       // UserDetailsImpl detailUser=new UserDetailsImpl(userService.findByUsername(principal.getName()));
       /// return new ResponseEntity<>(detailUser,HttpStatus.OK);
        userService.findByUsername(principal.getName()).setFailedAttempt(0);
        return new ResponseEntity<>(userService.findByUsername(principal.getName()),HttpStatus.OK);
    }

    @GetMapping("api/user/all")
    public ResponseEntity<?> getAllUsers(){
        return ResponseEntity.ok(userService.findAllUsers());
    }

    @GetMapping("api/user/res")
    public ResponseEntity <?> home(@AuthenticationPrincipal UserDetailsImpl userDetails) {
            return ResponseEntity.ok(userDetails.getUsername());
    }
}




