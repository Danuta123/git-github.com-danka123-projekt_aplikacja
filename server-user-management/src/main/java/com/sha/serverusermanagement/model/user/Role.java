package com.sha.serverusermanagement.model.user;

public enum Role {
    USER,
    ADMIN
}
