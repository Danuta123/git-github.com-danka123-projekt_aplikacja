package com.sha.serverusermanagement.service.user;
import com.sha.serverusermanagement.model.note.Note;
import com.sha.serverusermanagement.model.user.User;

import java.util.List;

public interface UserService {
    User save(User user);

    User findByUsername(String username);

    List<String> findUsers(List<Long> idList);

    List<User> findAllUsers();
}
