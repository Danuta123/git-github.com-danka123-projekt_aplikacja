import { Component, OnInit, ViewChild  } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {User} from '../../model/user';
import {MatPaginator, MatTableDataSource, MatSort} from '@angular/material';
import {Router} from '@angular/router';
import {Note} from '../../model/note';
import {NoteWrapper} from '../../model/noteWrapper';
import {DecryptWrapper} from '../../model/decryptWrapper';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.css']
})

export class NoteComponent implements OnInit {
  currentUser: User;
  note: Note = new Note();
  noteWrapper: NoteWrapper = new NoteWrapper();

  decryptWrapper: DecryptWrapper = new DecryptWrapper();
  decryptedText: string;

  errorMessage: string;
  displayedColumns: string[] = ['text', 'owner', 'status', 'id'];

  noteList: Array<Note>;
  dataSource: MatTableDataSource<User> = new MatTableDataSource();

  publicNoteList: Array<Note>;
  publicDataSource: MatTableDataSource<User> = new MatTableDataSource();

  sentNoteList: Array<Note>;
  sentDataSource: MatTableDataSource<User> = new MatTableDataSource();


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator2: MatPaginator;
  @ViewChild(MatSort) sort2: MatSort;

  constructor(private authService: AuthService, private router: Router) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  ngAfterViewInit() {
    this.publicDataSource.paginator = this.paginator;
    this.publicDataSource.sort = this.sort;
  }

  ngOnInit() {
    this.getUserNotes(this.currentUser);
    this.getPublicNotes();
    this.getSentNotes(this.currentUser.username);
  }

  addNote() {
    this.note.owner = this.currentUser.username;
    this.authService.addNote(this.note).subscribe(data => {
        this.router.navigate(['/profile']);
      },
      err => {
        this.errorMessage = 'Nie udalo sie dodac notatki, sprawdź poprawność danych';
      });
  }

  getUserNotes(user: User) {
    this.authService.getUserNotes(user.username).subscribe(data => {
      this.noteList = data;
      this.dataSource.data = data;
    },
    err => {
      this.errorMessage = 'Uzytkownik nie ma jeszcze notatek';
    });
  }

  getSentNotes(username: string) {
    this.authService.getSentNotes(username).subscribe(data => {
        this.sentNoteList = data;
        this.sentDataSource.data = data;
      },
      err => {
        this.errorMessage = 'Uzytkownik nie otrzymał notatek';
      });
  }

  getPublicNotes() {
    this.authService.getPublicNotes().subscribe(data => {
      this.publicNoteList = data;
      this.publicDataSource.data = data;
    },
      err => {
        this.errorMessage = 'Nie udało się wczytać publicznych notatek';
      });
  }

  sendNote() {
    this.noteWrapper.owner = this.currentUser.username;
    this.noteWrapper.status = 'SHARED';
    this.authService.sendNote(this.noteWrapper).subscribe(data => {
        this.router.navigate(['/profile']);
      },
      err => {
        this.errorMessage = 'Nie udalo sie udostępnić notatki';
      });
  }

  decryptNote() {
    this.decryptedText = 'xx';
    this.authService.decryptNote(this.decryptWrapper).subscribe(data => {
        this.decryptedText = data.decrypted;
      },
      err => {
        this.errorMessage = 'Nie udalo sie odszyfrować notatki, spróbuj jeszcze raz!';
      });
  }

}
